from bs4 import BeautifulSoup
import requests

try:
    session = requests.session()
    resp = session.get("http://dizison4.com/diziler/")
    category_html = BeautifulSoup(resp.content, "html.parser")
    authorization_token = "ccc05652e932244f7f5eb26d321038db"

    for cat in category_html.select(
            'div#wrapper > nav.navbar.navbar-dp > div.hidden-xs.hidden-sm > div#acat > ul.acat-list > li > a'):
        movie_name = cat.get_text(strip=True)
        category_link = cat.attrs["href"]

        movie_page = session.get(category_link)
        x = movie_page.content.decode().split('psscroll')[1]
        x_resp = BeautifulSoup(x, "html.parser")

        movie_page_html = BeautifulSoup(movie_page.content, "html.parser")
        data = []

        for mp in movie_page_html.select(
                'div#wrapper > div.section.def_section > div.wrapper.section_wrapper > div > div.single_content'):
            movie_img = mp.select_one('div.cover-area > img').attrs["src"]

            movie_type = mp.select('div.top-terms > a')[0].get_text(strip=True)  # tür ve kategori
            imdb_score = mp.select_one('div.item-image > a > div.rating-r > span.dip').get_text(strip=True)
            description = mp.select_one('div.cap-desc > div.diziwrapper > div.dizihakkinda').get_text(strip=True)

            vision_date = mp.select('div.nano > div > div.genre-item')[2].select_one('a').get_text(strip=True)

            movie_p = x_resp.find_all('a')
            for i in movie_p:
                m_p = i.attrs["href"]

                try:
                    mp_req = session.get(m_p + "?alternatif=3")
                except requests.exceptions.RequestException:
                    mp_req = session.get(m_p + "?alternatif=3")

                mp_resp = BeautifulSoup(mp_req.content, "html.parser")

                try:
                    season_name = mp_resp.select_one(
                        'div.left_posts > div.single_content > div.stitle.type1 > h1 > span.season-name').get_text(
                        strip=True)
                except AttributeError:
                    continue

                try:
                    episode_name = mp_resp.select_one(
                        'div.left_posts > div.single_content > div.stitle.type1 > h1 > span.episode-name').get_text(
                        strip=True)
                except AttributeError:
                    continue

                try:
                    xp = mp_req.content.decode().split('player-top-ad')[1]
                    xr = BeautifulSoup(xp, "html.parser")
                except IndexError:
                    continue

                try:
                    movie_path_search = xr.select('div.tabcontent')[1]
                except IndexError:
                    continue

                try:
                    movie_path = movie_path_search.select_one('iframe').attrs["src"]
                except AttributeError:
                    movie_path_search = xr.select('div.tabcontent')[0]
                    movie_path = movie_path_search.select_one('iframe').attrs["src"]

                data_dict = {
                    "season_name": season_name,
                    "epsiode_name": episode_name,
                    "movie_path": movie_path
                }
                data.append(data_dict)
            c_post = requests.post(
                "http://127.0.0.1:8000/category/create",
                json={"category_name": movie_type},
                headers={
                    'Authorization': authorization_token
                })
            print(movie_type, "c==>", c_post.status_code)
            movie_list = {
                "movie_name": movie_name,
                "movie_img": movie_img,
                "movie_type": movie_type,
                "imdb": imdb_score,
                "description": description,
                "vision_date": vision_date,
                "movies": data
            }
            if movie_list["movies"] == []:
                continue

            m_post = requests.post(
                "http://127.0.0.1:8000/series/create",
                json=movie_list,
                headers={
                    'Authorization': authorization_token
                }
            )
            print("m==>", m_post.status_code)
            # print(movie_list)
except requests.exceptions.RequestException:
    print("bay bay olduk :)")
    pass
