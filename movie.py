from bs4 import BeautifulSoup
import requests
import settings

session = requests.session()
resp = session.get(settings.URL["home_url"]["url"])
category_html = BeautifulSoup(resp.content, "html.parser")
authorization_token = "23d86ce41ea2c1d7c4414a6a98585e7b"

for cat in category_html.select("div.sayfa-sag > div#sag-kategori-tablo"):
    for category in cat.select('div.kategoriler.ust > ul.ek-liste > li > a '):
        category_name = category.get_text(strip=True)
        category_link = category.attrs['href']

        category_movie_resp = session.post(settings.URL["home_url"]["url"] + category_link)
        movie_ex = BeautifulSoup(category_movie_resp.content, "html.parser")

        for movie_info in movie_ex.select('div.sayfa-sol > div#icerik > div.film-k.kutu-icerik.kat'):
            m_url = movie_info.select_one('div.play.fa.fa-play-circle > a').attrs['href']
            quality = movie_info.select_one('div.kalite').get_text(strip=True)
            imdb = movie_info.select_one('div.imdb > b').get_text(strip=True)
            m_type = movie_info.select_one('div.bilgi.gizle > ul.ek > li.tur').get_text(strip=True)
            vision_date = movie_info.select('div.bilgi.gizle > ul.ek > li.a')[1].get_text(strip=True)
            time = movie_info.select('div.bilgi.gizle > ul.ek > li')[4].get_text(strip=True)

            movie_url = settings.URL["home_url"]["url"] + m_url

            movie_resp = session.post(movie_url)
            movie_data = BeautifulSoup(movie_resp.content, 'html.parser')
            movie_img = movie_data.find("meta", property="og:image")["content"]

            for movies in movie_data.select('div#sayfa > div#sayfa-ic'):

                movie_name = movies.select_one('div#film-tab > h1').get_text(strip=True)
                try:
                    movie_id = \
                        movies.select('div#film-tab > div.tab-cizgi > ul.tab-baslik.dropit > li.parttab')[1].attrs[
                            "id"].replace(
                            "p", "").strip()

                except IndexError:
                    continue

                req1 = session.post(movie_url, data={"pid": movie_id})
                movie_soup = BeautifulSoup(req1.content, "html.parser")
                player_url = movie_soup.select_one('iframe').attrs["src"]

                if "https:" not in player_url:
                    movie_path = "https:{}".format(player_url)

                if "youtube" in player_url or "vshare" in player_url:
                    continue

                movie_description = movies.select_one(
                    'div.izle-ust > div.resim-bg.test > div.resim-bg-ic > div.slayt-tablo > div.slayt-orta > div.slayt-aciklama').get_text(
                    strip=True)

                c_post = requests.post(
                    "http://127.0.0.1:8000/category/create",
                    json={"category_name": category_name},
                    headers={
                        'Authorization': authorization_token
                    })
                print(category_name, "c==>", c_post.status_code)
                movie_list = {
                    'category': category_name,
                    'movie_name': movie_name.replace('izle', '').strip(),
                    'movie_path': movie_path,
                    'movie_description': movie_description,
                    'image': movie_img,
                    'movie_type': m_type.split(':')[- 1],
                    'movie_time': time.split(':')[- 1],
                    'vision_date': vision_date.split(':')[- 1],
                    'quality': quality,
                    'imdb': float(imdb)
                }

                m_post = requests.post(
                    "http://127.0.0.1:8000/movie/create",
                    json=movie_list,
                    headers={
                        'Authorization': authorization_token
                    }
                )
                print("m==>", m_post.status_code)


